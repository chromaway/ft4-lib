## [0.3.1r] - 2024-01-19

### Changed

- Validate `register_crosschain_asset parameters`
- Added `op_index` to the `get_transfer_history` response
