## [0.3.1] - 2024-01-19

### Changed
- `Amount` type is now exported
- A new field `opIndex` was added to the `TransferHistoryEntry` type which can be passed into `getTransferDetails` and `getTransferDetailsByAsset`
