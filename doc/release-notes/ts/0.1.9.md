## [0.1.9] - 2023-11-14

### Changed 
- Use Rollup for packaging, produce output for ECMAScript, Common.JS and UMD. 
- Move `cryptoUtils` module into main library (imports needs to be updated).
- Export `createAmountFromBalance` and `createAssetObject` functions.
