import { useChromiaNode } from "@ft4-test/util";
import {
  AuthDataService,
  relativeBlockHeight,
  ttlLoginRule,
} from "@ft4/authentication";
import { and, lessOrEqual, lessThan, opCount } from "@ft4/accounts";
import { createAuthDataService, createConnection } from "@ft4/ft-session";

describe("Test auth data service", () => {
  let _authDataService: AuthDataService;
  const getClient = useChromiaNode();

  beforeAll(async () => {
    const client = getClient();
    _authDataService = createAuthDataService(createConnection(client));
  });

  it("gets operations scope auth handler when operation scope auth handler defined", async () => {
    const authHandler = await _authDataService.getAuthHandlerForOperation(
      "test_empty_auth_message",
    );
    expect(authHandler!.name).toEqual("test_empty_auth_message");
  });

  it("gets login config with ttl rule", async () => {
    const config = await _authDataService.getLoginConfig(
      "custom_config_with_ttl",
    );

    expect(config).toEqual({
      flags: ["Z"],
      rules: ttlLoginRule(10),
    });
  });

  it("gets config with simple rule", async () => {
    const config = await _authDataService.getLoginConfig(
      "custom_config_with_simple_rule",
    );
    expect(config).toEqual({
      flags: ["W"],
      rules: lessThan(relativeBlockHeight(5)),
    });
  });

  it("gets config with complex rules", async () => {
    const config = await _authDataService.getLoginConfig(
      "custom_config_with_complex_rule",
    );

    expect(config).toEqual({
      flags: ["A", "Z"],
      rules: and(lessThan(relativeBlockHeight(5)), lessOrEqual(opCount(7))),
    });
  });

  it("generates auth message template for operation with undefined arguments", async () => {
    const authMessageTemplate = await _authDataService.getAuthMessageTemplate({
      name: "foo",
    });

    const expectedMessageTemplate = `Blockchain:
{blockchain_rid}

Please sign the message to call
operation:
- foo

Nonce: {nonce}`;

    expect(authMessageTemplate).toEqual(expectedMessageTemplate);
  });
});
