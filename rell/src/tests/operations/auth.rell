module;

import ^^^.lib.ft4.admin;
import acc: ^^^.lib.ft4.accounts;
import auth: ^^^.lib.ft4.auth;


function empty_message(gtv) {
  return "";
}

@extend(auth.auth_handler)
function () = auth.add_auth_handler(
    scope = rell.meta(test_empty_auth_message).mount_name,
    flags = ["E"],
    message = empty_message(*)
);
operation test_empty_auth_message() {
  auth.authenticate();
}

function test_perform_large_transfer_message(gtv) {
  val args = struct<test_perform_large_transfer>.from_gtv(gtv);
  return "Do you want to send %s CHR to %s?".format(args.amount, args.to);
}

@extend(auth.auth_handler)
function () = auth.add_auth_handler(
    scope = rell.meta(test_perform_large_transfer).mount_name,
    flags = ["A"],
    message = test_perform_large_transfer_message(*)
);
operation test_perform_large_transfer(
  amount: integer,
  to: text
) {
  auth.authenticate();
}

function test_auth_with_resolver_resolver(args: gtv, account_id: byte_array, ad_ids: list<byte_array>): byte_array? {
  val idx = struct<test_auth_with_resolver>.from_gtv(args).ad_to_select;
  return ad_ids[idx];
}

@extend(auth.auth_handler)
function () = auth.add_auth_handler(
  scope = rell.meta(test_auth_with_resolver).mount_name,
  flags = [],
  resolver = test_auth_with_resolver_resolver(*)
);
operation test_auth_with_resolver(ad_to_select: integer) {
  auth.authenticate();
}

@extend(auth.auth_handler)
function () = auth.add_auth_handler(
  scope = rell.meta(extra_signatures_op).mount_name,
  flags = []
);

operation extra_signatures_op(signers: list<byte_array>) {
  auth.verify_signers(signers);
}

// This op cannot be called after an auth op, used to test blacklist.
operation unauthable_op() {}