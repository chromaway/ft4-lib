@test
module;

import lib.ft4.auth;
import lib.ft4.assets;
import lib.ft4.utils;
import lib.ft4.accounts;

import ^^.extensions.*;
import test: lib.ft4.test.core;
import ^.helpers;
import common_helpers: ^^.common.helpers;

function test_history() {
    val alice = test.register_alice();
    val bob = test.register_bob();

    val asset = test.create_asset("test", "t", 5);
    test.mint(alice.account, asset, 1);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .sign(rell.test.keypairs.alice)
        .run();

    val filter = assets.filter(null);
    val start_cursor = utils.encode_cursor(utils.page_cursor());
    val response_small = assets.external.get_transfer_history(alice.account.id, filter, 10, start_cursor);
    val response_big = assets.external.get_transfer_history(alice.account.id, filter, 110, start_cursor);
    val expected_data = assets.extract_data_from_transfer_history_list(
            assets.transfer_history_entry @* {
                accounts.account @ { .id == alice.account.id }
            } (@sort_desc @omit .rowid, $) limit 2
        ) @* {} (.data);
    val expected = utils.paged_result(
        next_cursor = null, 
        data = expected_data
    );
    
    assert_equals(response_small, expected);
    assert_equals(response_big, expected);
    assert_true(response_small!!.data.size() == 2);
    assert_true(response_big!!.data.size() == 2);
}

function test_history_pagination() {
    val alice = test.register_alice();
    val bob = test.register_bob();

    val asset = test.create_asset("test", "t", 0);

    test.mint(alice.account, asset, 6L); //adds one entry to the history

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 2L))
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 2L))
        .sign(rell.test.keypairs.alice)
        .run();
    
    val filter = assets.filter(null);
    var response = assets.external.get_transfer_history(alice.account.id, filter, 2, utils.encode_cursor(utils.page_cursor()));
    var expected_result = assets.extract_data_from_transfer_history_list(
            assets.transfer_history_entry @* {
                accounts.account @ { .id == alice.account.id }
            } (@sort_desc @omit .rowid, $) limit 2
        );
    var expected_data = expected_result @* {} (.data);
    var next_cursor = utils.encode_cursor(utils.page_cursor(expected_result[1].rowid));
    var expected = utils.paged_result(
        next_cursor = next_cursor,
        data = expected_data
    );
    
    assert_true(expected_result.size() == 2);
    assert_equals(response, expected);

    //vvv amount, checks if they're ordered LIFO vvv
    val amount1 = map<text, gtv>.from_gtv(expected_data[0])['delta'];
    assert_equals(amount1, (2L).to_gtv());

    response = assets.external.get_transfer_history(alice.account.id, filter, 2, next_cursor);
    expected_result = assets.extract_data_from_transfer_history_list(
            assets.transfer_history_entry @* {
                accounts.account @ { .id == alice.account.id }
            } (@sort_desc @omit .rowid, $) offset 2 limit 2
        );
    expected_data = expected_result @* {} (.data);
    next_cursor = utils.encode_cursor(utils.page_cursor(expected_result[1].rowid));;
    expected = utils.paged_result(
        next_cursor = next_cursor,
        data = expected_data
    );
    assert_equals(expected_result.size(), 2);
    assert_equals(response, expected);

    val amount2 = map<text, gtv>.from_gtv(expected_data[0])['delta'];
    assert_equals(amount2, (1L).to_gtv());

    val amount3 = map<text, gtv>.from_gtv(expected_data[1])['delta'];
    assert_equals(amount3, (2L).to_gtv());

    response = assets.external.get_transfer_history(alice.account.id, filter, 2, next_cursor);
    expected_result = assets.extract_data_from_transfer_history_list(
            assets.transfer_history_entry @* {
                accounts.account @ { .id == alice.account.id }
            } (@sort_desc @omit .rowid, $) offset 4 limit 2
        );
    expected_data = expected_result @* {} (.data);
    expected = utils.paged_result(
        next_cursor = null, 
        data = expected_data
    );
    assert_true(expected_result.size() == 1);
    assert_equals(response, expected);
}

function test_history_filtering() {
    val alice = test.register_alice();
    val bob = test.register_bob();

    val asset = test.create_asset("test", "t", 0);

    test.mint(alice.account, asset, 5L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .sign(rell.test.keypairs.alice)
        .run();

    val cursor = utils.encode_cursor(utils.page_cursor());
    assert_equals(assets.external.get_transfer_history(alice.account.id, assets.filter(assets.transfer_type.sent), 10, cursor)!!.data.size(), 1);
    assert_equals(assets.external.get_transfer_history(alice.account.id, assets.filter(assets.transfer_type.received), 10, cursor)!!.data.size(), 1);
    assert_equals(assets.external.get_transfer_history(bob.account.id, assets.filter(assets.transfer_type.sent), 10, cursor)!!.data.size(), 0);
    assert_equals(assets.external.get_transfer_history(bob.account.id, assets.filter(assets.transfer_type.received), 10, cursor)!!.data.size(), 1);
}

function test_transfer_history_entry() {
    val alice = test.register_alice();
    val bob = test.register_bob();

    val asset = test.create_asset("test", "t", 0);

    test.mint(alice.account, asset, 5L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .sign(rell.test.keypairs.alice)
        .run();
    
    val entry = assets.transfer_history_entry @* {}[0];
    assert_equals(
        assets.external.get_transfer_history_entry(entry.rowid),
        (
            id = entry.rowid,
            delta = entry.delta,
            decimals = entry.asset.decimals,
            asset = (
                id = entry.asset.id,
                name = entry.asset.name,
                symbol = entry.asset.symbol,
                decimals = entry.asset.decimals,
                blockchain_rid = entry.asset.issuing_blockchain_rid,
                icon_url = entry.asset.icon_url,
                type = assets.ASSET_TYPE_FT4,
                supply = entry.asset.total_supply
            ),
            is_input = entry.is_input,
            timestamp = entry.transaction.block.timestamp,
            block_height = entry.transaction.block.block_height,
            tx_rid = entry.transaction.tx_rid,
            tx_data = entry.transaction.tx_data,
            operation_name = "ft4.admin.mint",
            op_index = entry.op_index,
            is_crosschain = false
        )
    );
}

function test_transfer_history_operation_name() {
    val alice = test.register_alice();
    val bob = test.register_bob();

    val asset = test.create_asset("test", "t", 0);

    test.mint(alice.account, asset, 5L);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .sign(rell.test.keypairs.alice)
        .run();

    val entries = assets.transfer_history_entry @* {};
    assert_equals(
        assets.external.get_transfer_history_entry(entries[0].rowid)!!.operation_name, "ft4.admin.mint"
    );
    assert_equals(
        assets.external.get_transfer_history_entry(entries[1].rowid)!!.operation_name, "ft4.transfer"
    );
}

function test_transfer_details() {
    val alice = test.register_alice();
    val bob = test.register_bob();

    val asset = test.create_asset("test", "t", 5);
    test.mint(alice.account, asset, 1);

    val asset2 = test.create_asset("test2", "t2", 5);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .sign(rell.test.keypairs.alice)
        .run();

    val tx_rid = test.get_last_transaction().tx_rid;
    val last_two_transfer_history_entries = common_helpers.get_last_two_transfer_history_entries();

    val expected = [
        assets.transfer_detail(
            rowid = last_two_transfer_history_entries[1].rowid,
            blockchain_rid = chain_context.blockchain_rid,
            account_id = alice.account.id,
            asset_id = asset.id,
            delta = 1L,
            is_input = true,
            op_index = last_two_transfer_history_entries[1].op_index,
            transaction_rid = last_two_transfer_history_entries[1].transaction.tx_rid
        ),
        assets.transfer_detail(
            rowid = last_two_transfer_history_entries[0].rowid,
            blockchain_rid = chain_context.blockchain_rid,
            account_id = bob.account.id,
            asset_id = asset.id,
            delta = 1L,
            is_input = false,
            op_index = last_two_transfer_history_entries[0].op_index,
            transaction_rid = last_two_transfer_history_entries[0].transaction.tx_rid
        )
    ];

    assert_equals(assets.external.get_transfer_details(tx_rid, 1), expected);
    assert_equals(assets.external.get_transfer_details_by_asset(tx_rid, 1, asset.id), expected);
    assert_equals(assets.external.get_transfer_details_by_asset(tx_rid, 1, asset2.id), list<assets.transfer_detail>());
}

function test_history_by_asset() {
    val alice = test.register_alice();
    val bob = test.register_bob();

    val asset = test.create_asset("test", "t", 5);
    test.mint(alice.account, asset, 1);

    rell.test.tx()
        .op(auth.external.ft_auth(alice.account.id, alice.auth_descriptor_id))
        .op(assets.external.transfer(bob.account.id, asset.id, 1L))
        .sign(rell.test.keypairs.alice)
        .run();

    val start_cursor = utils.encode_cursor(utils.page_cursor());
    val response = assets.external.get_transfer_history_from_height(3, asset.id, 10, start_cursor);
    val expected_data = assets.extract_data_from_transfer_history_list(
            assets.transfer_history_entry @* {
                asset
            } (@sort_desc @omit .rowid, $) limit 2
        ) @* {} (.data);
    val expected = utils.paged_result(
        next_cursor = null,
        data = expected_data
    );

    assert_equals(response.data.size(), 2);
    assert_equals(response, expected);
}
