@test module;

import ^^^.lib.ft4.assets;
import ^^^.lib.ft4.admin;
import test: lib.ft4.test.core;

val alice = rell.test.keypairs.alice;
val bob = rell.test.keypairs.bob;

function test_an_asset_can_be_created() {
  test.create_asset("a", "a", 0);
  test.create_asset("b", "b", 1);
  test.create_asset("c", "c", 50);
  test.create_asset("d", "d", 78);
}

function test_an_invalid_asset_cannot_be_created() {
  val error = "Decimals must be between 0 and 78 (included)";
  admin.external.register_asset("e", "e", -1, "")
    .sign(test.admin_priv_key())
    .run_must_fail(error);
  admin.external.register_asset("f", "f", 79, "")
    .sign(test.admin_priv_key())
    .run_must_fail(error);
}

function test_asset_amounts_are_correctly_formatted() {
  assert_equals(assets.format_amount_with_decimals(1L, 0), "1");
  assert_equals(assets.format_amount_with_decimals(10L, 0), "10");
  assert_equals(assets.format_amount_with_decimals(100000000L, 0), "100000000");
  assert_equals(assets.format_amount_with_decimals(1L, 1), "0.1");
  assert_equals(assets.format_amount_with_decimals(10L, 1), "1.0");
  assert_equals(assets.format_amount_with_decimals(100000000L, 1), "10000000.0");
  assert_equals(assets.format_amount_with_decimals(1L, 6), "0.000001");
  assert_equals(assets.format_amount_with_decimals(10L, 6), "0.000010");
  assert_equals(assets.format_amount_with_decimals(100000000L, 6), "100.000000");
}