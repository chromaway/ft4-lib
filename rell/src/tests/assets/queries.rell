@test module;

import lib.ft4.admin;
import lib.ft4.assets;
import test: lib.ft4.test.core;

import ^.helpers;
import ^.mocks;
import common_helpers: ^^.common.helpers;

val too_big = assets.max_asset_amount + 1;

function test_get_asset_balance_no_account() {
    val alice = test.register_alice().account.id;
    val asset = test.create_asset("test", "TST", 0).id;

    assert_null(assets.external.get_asset_balance(x"abcd", asset));
}

function test_get_asset_balance_no_asset() {
    val alice = test.register_alice().account.id;
    val asset = test.create_asset("test", "TST", 0).id;

    assert_null(assets.external.get_asset_balance(alice, x"abcd"));
}

function test_get_asset_balance_zero() {
    val alice = test.register_alice().account.id;
    val asset = test.create_asset("test", "TST", 0).id;

    assert_null(assets.external.get_asset_balance(alice, asset));
}

function test_get_asset_balance_success() {
    val alice = test.register_alice().account.id;
    val asset = test.create_asset("test", "TST", 0).id;

    admin.external.mint(alice, asset, 10L)
        .sign(test.admin_priv_key())
        .run();
    
    assert_equals(assets.external.get_asset_balance(alice, asset)?.amount, 10L);
}