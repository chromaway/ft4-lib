@test
module;

import lib.ft4.auth;
import lib.ft4.accounts;
import lib.ft4.accounts.strategies;
import lib.ft4.accounts.strategies.open;
import lib.ft4.test.core. { evm_sign };

import ^^.operations;

function test_account_can_be_registered_with_open_strategy() {
    val auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.keypairs.alice.pub,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    rell.test.tx()
        .op(open.ras_open(auth_descriptor))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.alice)
        .run();

    val account = accounts.account @? { .id == rell.test.keypairs.alice.pub.hash() };

    assert_not_null(account);
}

function test_account_can_be_registered_with_open_strategy_and_evm_signature() {
    val evm_address = crypto.eth_pubkey_to_address(rell.test.keypairs.alice.pub);

    val auth_descriptor = accounts.single_sig_auth_descriptor(
        evm_address,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    val open = open.ras_open(auth_descriptor);
    val register_account = strategies.external.register_account();

    val message = strategies.get_register_account_message(
        open.to_gtx_operation(),
        strategies.external.register_account().to_gtx_operation()
    );
    
    val signature = evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([evm_address], [signature]))
        .op(open)
        .op(strategies.external.register_account())
        .run();

    val account = accounts.account @? { .id == evm_address.hash() };

    assert_not_null(account);
}

function test_account_can_be_registered_with_custom_operation() {
    val name = "Alice (the neighbor)";

    val auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.keypairs.alice.pub,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    rell.test.tx()
        .op(open.ras_open(auth_descriptor))
        .op(operations.register_player(name))
        .sign(rell.test.keypairs.alice)
        .run();

    val account = accounts.account @? { .id == rell.test.keypairs.alice.pub.hash() };
    assert_not_null(account);
    val player_name = operations.player @? { account } ( .name );
    assert_equals(player_name, name);
}

function test_account_can_be_registered_with_custom_operation_and_evm_signature() {
    val name = "Alice (the neighbor)";

    val evm_address = crypto.eth_pubkey_to_address(rell.test.keypairs.alice.pub);

    val auth_descriptor = accounts.single_sig_auth_descriptor(
        evm_address,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    val open = open.ras_open(auth_descriptor);
    val register_player = operations.register_player(name);

    val message = strategies.get_register_account_message(open.to_gtx_operation(), register_player.to_gtx_operation());
    val signature = evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([evm_address], [signature]))
        .op(open)
        .op(register_player)
        .run();

    val account = accounts.account @? { .id == evm_address.hash() };
    assert_not_null(account);
    val player_name = operations.player @? { account } ( .name );
    assert_equals(player_name, name);
}

function test_account_account_registration_fails_with_unknown_custom_operation_and_evm_signature() {
    val name = "Alice (the neighbor)";

    val evm_address = crypto.eth_pubkey_to_address(rell.test.keypairs.alice.pub);

    val auth_descriptor = accounts.single_sig_auth_descriptor(
        evm_address,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    val open = open.ras_open(auth_descriptor);
    val register_player2 = operations.register_player2(name);

    val message = strategies.get_register_account_message(open.to_gtx_operation(), register_player2.to_gtx_operation());
    val signature = evm_sign(message, rell.test.keypairs.alice.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([evm_address], [signature]))
        .op(open)
        .op(register_player2)
        .run_must_fail("Cannot authorize operation <register_player2> with evm_signatures");
}

function test_account_registration_fails_with_invalid_ft_signature() {
    val auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.keypairs.alice.pub,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    rell.test.tx()
        .op(open.ras_open(auth_descriptor, null))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.bob)
        .run_must_fail("Missing signature for public key <%s>".format(rell.test.pubkeys.alice));
}

function test_account_registration_fails_with_missing_disposable_ft_signature() {
    val main_auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.pubkeys.alice,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    val disposable_auth_descriptor = accounts.single_sig_auth_descriptor(rell.test.pubkeys.bob, set([accounts.auth_flags.TRANSFER]));

    rell.test.tx()
        .op(open.ras_open(main_auth_descriptor, disposable_auth_descriptor))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.alice)
        .run_must_fail("Missing signature for public key <%s>".format(rell.test.pubkeys.bob));
}

function test_account_registration_fails_with_invalid_evm_signature() {
    val evm_address = crypto.eth_pubkey_to_address(rell.test.keypairs.alice.pub);

    val auth_descriptor = accounts.single_sig_auth_descriptor(
        evm_address,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    val open = open.ras_open(auth_descriptor);
    val register_account = strategies.external.register_account();

    val message = strategies.get_register_account_message(
        open.to_gtx_operation(), 
        strategies.external.register_account().to_gtx_operation()
    );
    
    val signature = evm_sign(message, rell.test.keypairs.bob.priv);

    rell.test.tx()
        .op(auth.external.evm_signatures([evm_address], [signature]))
        .op(open)
        .op(strategies.external.register_account())
        .run_must_fail("Invalid signature for address <%s>".format(evm_address));
}

function test_account_id_does_not_change_if_signers_provided_in_different_orders() {
    val list1 = [
        rell.test.pubkeys.alice,
        rell.test.pubkeys.bob,
        rell.test.pubkeys.eve
    ];

    val list2 = [
        rell.test.pubkeys.bob,        
        rell.test.pubkeys.alice,
        rell.test.pubkeys.eve
    ];

    val list3 = [
        rell.test.pubkeys.eve,  
        rell.test.pubkeys.bob,        
        rell.test.pubkeys.alice
    ];

    assert_equals(
        accounts.get_account_id_from_signers(list1),
        accounts.get_account_id_from_signers(list2)
    );

    assert_equals(
        accounts.get_account_id_from_signers(list2),
        accounts.get_account_id_from_signers(list3)
    );    
}

function test_account_registration_fails_when_account_already_exists() {
    val auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.keypairs.alice.pub,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    rell.test.tx()
        .op(open.ras_open(auth_descriptor, null))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.alice)
        .run();

    val account = accounts.account @? { .id == rell.test.keypairs.alice.pub.hash() };

    assert_not_null(account);

    rell.test.tx()
        .op(open.ras_open(auth_descriptor, null))
        .op(strategies.external.register_account())
        .sign(rell.test.keypairs.alice)
        .nop()
        .run_must_fail("duplicate key value violates unique constraint");
}

function test_an_error_is_thrown_when_no_suceeding_op() {
    val auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.keypairs.alice.pub,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    rell.test.tx()
        .op(open.ras_open(auth_descriptor, null))
        .sign(rell.test.keypairs.alice)
        .run_must_fail("<ft4.ras_open> must be followed by a register account operation");
}

function test_an_error_is_thrown_when_not_followed_by_register_account_op() {
    val auth_descriptor = accounts.single_sig_auth_descriptor(
        rell.test.keypairs.alice.pub,
        set([accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER])
    );

    rell.test.tx()
        .op(open.ras_open(auth_descriptor, null))
        .nop()
        .sign(rell.test.keypairs.alice)
        .run_must_fail("<ft4.ras_open> operation must be followed by a register account operation, not <nop>");
}
