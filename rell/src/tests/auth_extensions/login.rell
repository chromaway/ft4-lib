@test
module;

import lib.ft4.auth;
import lib.ft4.accounts;

/*
 *  Tests login_config extension defined in tests/extensions/auth.rell
 */
function test_default_login_config_can_be_overriden() {
    val config = auth.external.get_login_config("default");

    assert_equals(config, auth._login_config(flags = ["B"]));
}

function test_querying_login_config_without_name_returns_default_config() {
    val config = auth.external.get_login_config();

    assert_equals(config, auth._login_config(flags = ["B"]));
}

function test_custom_login_config_can_be_fetched() {
    val config = auth.external.get_login_config("custom_config");

    assert_equals(config, auth._login_config(flags = ["X", "Y"]));
}

function test_custom_login_config_can_have_ttl() {
    val config = auth.external.get_login_config("custom_config_with_ttl");

    assert_equals(
        config,
        auth._login_config(
            flags = ["Z"],
            rules = auth.ttl(10)
        )
    );
}

function test_custom_login_config_can_have_simple_rule() {
    val config = auth.external.get_login_config("custom_config_with_simple_rule");

    assert_equals(
        config,
        auth._login_config(
            flags = ["W"],
            rules = auth.login_simple_rule(
                auth.less_than(auth.relative_block_height(5))
            )
        )
    );
}

function test_custom_login_config_can_have_complex_rule() {
    val config = auth.external.get_login_config("custom_config_with_complex_rule");

    assert_equals(
        config,
        auth._login_config(
            flags = [accounts.auth_flags.ACCOUNT, "Z"],
            rules = auth.login_rules([
                auth.less_than(auth.relative_block_height(5)),
                auth.less_or_equal(auth.op_count(7))
            ])
        )
    );
}

