@test module;

import ^^^.lib.ft4.accounts;
import auth: ^^^.lib.ft4.auth;
import test_operations: ^^.operations;
import test: lib.ft4.test.core;

function test_it_returns_null_if_no_ads_provided() {
    val args = struct<test_operations.test_auth.test_auth_with_resolver>(1);
    val selected_id = auth.external.get_first_allowed_auth_descriptor("test_auth_with_resolver", args.to_gtv(), x"", []);
    assert_null(selected_id);
}

function test_it_returns_first_ad_id_if_no_resolver() {
    val args = struct<test_operations.test_auth.test_auth_with_resolver>(1);
    val id1 = "1".to_bytes();
    val id2 = "2".to_bytes();
    val selected_id = auth.external.get_first_allowed_auth_descriptor("test_auth_without_resolver", args.to_gtv(), x"", [id1, id2]);
    assert_equals(selected_id, id1);
}

function test_it_returns_ad_id_selected_by_resolver_if_provided() {
    val args = struct<test_operations.test_auth.test_auth_with_resolver>(2);
    val id1 = "1".to_bytes();
    val id2 = "2".to_bytes();
    val id3 = "3".to_bytes();
    val selected_id = auth.external.get_first_allowed_auth_descriptor("test_auth_with_resolver", args.to_gtv(), x"", [id1, id2, id3]);
    assert_equals(selected_id, id3);
}

function test_get_first_allowed_auth_descriptor_by_signers_returns_auth_descriptor_id_when_flags_are_fullfilled() {
    val alice = test.register_account_open(rell.test.keypairs.alice, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER, "E"]);
    val result = auth.get_first_allowed_auth_descriptor_by_signers(
        op_name = "test_empty_auth_message",
        args = list<gtv>().to_gtv(),
        account_id = alice.account.id,
        signers = [rell.test.pubkeys.alice]);
    assert_equals(result, alice.auth_descriptor_id);
}

function test_get_first_allowed_auth_descriptor_by_signers_returns_auth_descriptor_id_when_extra_signers_are_passed() {
    val alice = test.register_account_open(rell.test.keypairs.alice, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER, "E"]);
    val result = auth.get_first_allowed_auth_descriptor_by_signers(
        op_name = "test_empty_auth_message",
        args = list<gtv>().to_gtv(),
        account_id = alice.account.id,
        signers = [rell.test.pubkeys.bob, rell.test.pubkeys.alice]);
    assert_equals(result, alice.auth_descriptor_id);
}

function test_get_first_allowed_auth_descriptor_by_signers_returns_valid_auth_descriptor_id() {
    val alice = test.register_account_open(rell.test.keypairs.alice, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER, "E"]);
    val bob = test.register_account_open(rell.test.keypairs.bob, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
    val result = auth.get_first_allowed_auth_descriptor_by_signers(
        op_name = "test_empty_auth_message",
        args = list<gtv>().to_gtv(),
        account_id = alice.account.id,
        signers = [rell.test.pubkeys.alice, rell.test.pubkeys.bob]);
    assert_equals(result, alice.auth_descriptor_id);
}

function test_get_first_allowed_auth_descriptor_by_signers_returns_null_when_flags_are_not_fullfilled() {
    val alice = test.register_account_open(rell.test.keypairs.alice, [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER]);
    val result = auth.get_first_allowed_auth_descriptor_by_signers(
        op_name = "test_empty_auth_message",
        args = list<gtv>().to_gtv(),
        account_id = alice.account.id,
        signers = [rell.test.pubkeys.alice]);
    assert_null(result);
}
