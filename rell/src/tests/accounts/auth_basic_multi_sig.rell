@test
module;

import lib.ft4.accounts;
import accounts_ext: lib.ft4.external.accounts;
import test_operations: ^^.operations;
import test_op: lib.ft4.test.operations;
import test: lib.ft4.test.core;


function test_multi_sig_valid_1_of_1() {
    val alice = test.register_alice();

    rell.test.tx()
        .op(alice.ft_auth(test_op.authenticated_operation()))
        .sign(rell.test.keypairs.alice) 
        .run();
}

function test_multi_sig_valid_2_of_2_signs() {
    val account_data = test.register_account_with_multisig_auth_descriptor(
        test.create_multisig_auth_descriptor(
            2,
            [rell.test.keypairs.alice.pub, rell.test.keypairs.bob.pub],
            [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER],
        ),
        [rell.test.keypairs.alice, rell.test.keypairs.bob]
    );

    rell.test.tx()
        .op(account_data.ft_auth(test_op.authenticated_operation()))
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();
}

function test_multi_sig_valid_1_of_2_signs() {
    val account_data = test.register_account_with_multisig_auth_descriptor(
        test.create_multisig_auth_descriptor(
            1,
            [rell.test.keypairs.alice.pub, rell.test.keypairs.bob.pub],
            [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER],
        ),
        [rell.test.keypairs.alice, rell.test.keypairs.bob]
    );

    rell.test.tx()
        .op(account_data.ft_auth(test_op.authenticated_operation()))
        .sign(rell.test.keypairs.bob) // <-- Omitting Alice should work
        .run();

}

function test_multi_sig_valid_3_of_3_signs() {
    val account_data = test.register_account_with_multisig_auth_descriptor(
        test.create_multisig_auth_descriptor(
            3,
            [rell.test.keypairs.alice.pub, rell.test.keypairs.bob.pub, rell.test.keypairs.trudy.pub],
            [accounts.auth_flags.ACCOUNT, accounts.auth_flags.TRANSFER],
        ),
        [rell.test.keypairs.alice, rell.test.keypairs.bob, rell.test.keypairs.trudy]
    );

    rell.test.tx()
        .op(account_data.ft_auth(test_op.authenticated_operation()))
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .sign(rell.test.keypairs.trudy)
        .run();
}

function test_multi_sig_wrong_required_flags() {
    val ad = test.create_multisig_auth_descriptor(
        1,
        [rell.test.keypairs.bob.pub],
        ["X"],
    );
    val account_data = test.register_alice();

    rell.test.tx()
        .op(account_data.ft_auth(accounts_ext.add_auth_descriptor(ad)))
        .sign(rell.test.keypairs.alice)
        .sign(rell.test.keypairs.bob)
        .run();

    rell.test.tx()
        .op(test.ft_auth_operation_for(rell.test.keypairs.bob.pub))
        .op(test_op.authenticated_operation())
        .sign(rell.test.keypairs.bob)
        .run_must_fail("MISSING FLAGS: Some required flags [A] are missing on the (multi sig) Auth Descriptor.");
}
